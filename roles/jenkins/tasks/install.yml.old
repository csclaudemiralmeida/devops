---
- group:
    name: jenkins
    state: present

- name: Adding existing user jenkins to group docker
  user: name=jenkins
        groups=docker
        append=yes

- name: Create jenkins directory
  file: path=/var/lib/jenkins owner="{{jenkins_user}}" group="{{jenkins_group}}" mode=0770 state=directory recurse=yes


- name: check if jenkins exists
  command: docker volume inspect myvolume
  register: jenkinsvolume_exists
  failed_when: false

- name: create jenkins colume
  command: docker volume create --name jenkins
  when: jenkinsvolume_exists|failed



- name: create a jenkins container
  docker_container:
    name: jenkins
    image: jenkins
    state: started
    recreate: yes
    ports:
      - 8080:8080
      - 50000:50000
    volumes:
      - /var/lib/jenkins:/var/lib/jenkins
#      - /var/jenkins_home:/var/jenkins_home
    env:
      JAVA_OPTS: "-Djava.awt.headless=true -Xmx1024m"


- name: Aguarde até que Jenkins fique disponível
  wait_for: port=8080

- name: Remove initial Jenkins password
  file: name=/var/lib/jenkins/secrets/initialAdminPassword state=absent
#
- name: Create Jenkins admin password hash
  shell: echo -n "{{ jenkins_admin_password }}{ansible_jenkins}" | sha256sum - | awk '{ print $1; }'
  register: jenkins_password_hash
#
- name: Create admin user directory
  file: path="/var/lib/jenkins/users/admin" owner={{jenkins_user}} group={{jenkins_group}} mode=0755 state=directory recurse=yes
#
- name: Create admin
  template: src=admin-config.xml.j2 dest="/var/lib/jenkins/users/admin/config.xml" force=yes
  register: jenkins_admin_config
#
- name: Create config
  copy: src=config.xml dest="/var/lib/jenkins/config.xml"
  register: jenkins_config

- name: restart jenkins
  docker_container:
    name: jenkins
    image: jenkins
#    command:  ["/bin/tini", "--", "/usr/local/bin/jenkins.sh"]
    state: started
 #   recreate: yes
    restart: yes
    ports:
      - 8080:8080
      - 50000:50000
    volumes:
      - /var/lib/jenkins:/var/lib/jenkins
      #- /var/lib/jenkins:/var/jenkins_home
    env:
      JAVA_OPTS: "-Djava.awt.headless=true -Xmx1024m"

- name: Aguarde até que Jenkins fique disponível
  wait_for: port=8080

- name: Get Jenkins crumb
  uri:
    user: admin
    password: "{{ jenkins_admin_password }}"
    force_basic_auth: yes
    url: "http://127.0.0.1:8080/crumbIssuer/api/json"
    return_content: yes
  register: jenkins_crumb
  until: jenkins_crumb.content.find('Please wait while Jenkins is getting ready to work') == -1
  retries: 10
  delay: 5

- name: Set crumb token
  set_fact:
    jenkins_crumb_token: "{{ jenkins_crumb.json.crumbRequestField }}={{ jenkins_crumb.json.crumb }}"

- name: Get installed plugins
  uri:
    user: admin
    password: "{{ jenkins_admin_password }}"
    force_basic_auth: yes
    url: "http://127.0.0.1:8080/pluginManager/api/json?tree=plugins[shortName]&{{ jenkins_crumb_token }}"
    return_content: yes
  register: jenkins_installed_plugins


- name: Install plugins
  uri:
    user: admin
    password: "{{ jenkins_admin_password }}"
    force_basic_auth: yes
    url: "http://127.0.0.1:8080/pluginManager/install?plugin.{{ item }}.default=on&{{ jenkins_crumb_token }}"
    method: POST
    status_code: [200, 302]
  when: item not in jenkins_installed_plugins.json.plugins|map(attribute='shortName')|list
  with_items: "{{ jenkins_plugins }}"
  register: plugins_install
  until: plugins_install|success
  retries: 5
  delay: 20



- name: Wait for plugins to be installed
  uri:
    user: admin
    password: "{{ jenkins_admin_password }}"
    force_basic_auth: yes
    url: "http://127.0.0.1:8080/updateCenter/installStatus?{{ jenkins_crumb_token }}"
    return_content: yes
  register: jenkins_plugin_status
  until: "'Pending' not in jenkins_plugin_status.json.data.jobs|map(attribute='installStatus')"
  retries: 60
  delay: 10

- name: Check if we need to restart Jenkins to activate plugins
  uri:
    user: admin
    password: "{{ jenkins_admin_password }}"
    force_basic_auth: yes
    url: "http://127.0.0.1:8080/updateCenter/api/json\
    ?tree=restartRequiredForCompletion&{{ jenkins_crumb_token }}"
    return_content: yes
  register: jenkins_restart_required


#- name: restart  jenkins container
#  docker_container:
#    name: jenkins
#    image: jenkins
##    command:  ["/bin/tini", "--", "/usr/local/bin/jenkins.sh"]
#    state: started
# #   recreate: yes
#    restart: yes
#    ports:
#      - 8080:8080
#      - 50000:50000
#    volumes:
#      - /var/lib/jenkins:/var/lib/jenkins
#      - /var/lib/jenkins:/var/jenkins_home
#    env:
#      JAVA_OPTS: "-Djava.awt.headless=true -Xmx1024m"
#
#- name: Aguarde até que Jenkins fique disponível
#  wait_for: port=8080
