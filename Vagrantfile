# -*- mode: ruby -*-
# vi: set ft=ruby :

# Builds single Foreman server with katello and
# multiple Puppet Agent Nodes using JSON config file
# Claudemir Alemida Rosa
# Modified - 17/06/2017

# read vm and chef configurations from JSON files
nodes_config = (JSON.parse(File.read("nodes.json")))['nodes']

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  nodes_config.each do |node|
    node_name   = node[0] # name of node
    node_values = node[1] # content of node

    config.vbguest.auto_update = true
#    config.vbguest.iso_path = "http://download.virtualbox.org/virtualbox/%{version}/VBoxGuestAdditions_%{version}.iso"

    config.vm.box = node_values[':box']
    config.hostmanager.enabled = true
    config.hostmanager.ignore_private_ip = true
    config.hostmanager.enabled = true
    config.hostmanager.manage_guest = true
    config.hostmanager.manage_host = false

    config.vm.define node_name do |config|
      # configures all forwarding ports in JSON array
      ports = node_values['ports']
      ports.each do |port|
        config.vm.network :forwarded_port,
          guest: port.to_s,
          host:  port.to_s
      end

      config.vm.hostname = node_name
      config.ssh.forward_agent = true
      config.vm.network "private_network", type: "dhcp"
      config.hostmanager.ip_resolver = proc do |vm, resolving_vm|
            begin
                buffer = '';
                vm.communicate.execute("/sbin/ip add show") do |type, data|
                  buffer += data if type == :stdout
                end

                ips = []
                ifconfigIPs = buffer.scan(/inet addr:(\d+\.\d+\.\d+\.\d+)/)
                ifconfigIPs[0..ifconfigIPs.size].each do |ip|
                    ip = ip.first

                    next unless system "ping -c1 -t1 #{ip} > /dev/null"

                    ips.push(ip) unless ips.include? ip
                end

            end
        end
      config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", node_values[':memory']]
        vb.customize ["modifyvm", :id, "--name", node_name]
      end

    end
  end
end
